import Vue from "vue";
import VueMeta from "vue-meta";

// material-vue
import { MdApp, MdButton, MdField, MdProgress, MdRipple, MdSteppers, MdToolbar } from "vue-material/dist/components";
import "vue-material/dist/vue-material.min.css";
import "vue-material/dist/theme/default.css";

// app
import App from "./App.vue";
import "./style/index.scss";

// set as vue component
Vue.use(MdApp);
Vue.use(MdButton);
Vue.use(MdField);
Vue.use(MdProgress);
Vue.use(MdRipple);
Vue.use(MdSteppers);
Vue.use(MdToolbar);
Vue.use(VueMeta);

// render app and mount to DOM
new Vue({
  render: h => h(App)
}).$mount("#app");
