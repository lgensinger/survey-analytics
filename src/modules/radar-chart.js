import { RadarChart } from "@lgv/radar-chart";

/**
 * RC is an multivariate part-of-a-whole visualization.
 * @param {array} data - objects where each represents a path in the hierarchy
 * @param {integer} height - artboard height
 * @param {integer} width - artboard width
 */
class RC extends RadarChart {
    constructor(data, width, height, grid, padding, axesStart) {
        super(data, width, height, grid, padding, axesStart);
    }

    generateVisualization(styles) {

        // generate svg artboard
        this.artboard = this.generateArtboard(this.container);

        this.artboard.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", this.artboardWidth)
            .attr("height", this.artboardHeight)
            .style("fill", "grey");

        // generate quadrant x/y
        this.artboard
            .append("line")
            .attr("x1", 0)
            .attr("x2", this.artboardWidth)
            .attr("y1", this.artboardHeight / 2)
            .attr("y2", this.artboardHeight / 2)
            .attr("class", "quadrant-axis");

        this.artboard
            .append("line")
            .attr("x1", this.artboardWidth / 2)
            .attr("x2", this.artboardWidth / 2)
            .attr("y1", this.padding)
            .attr("y2", this.artboardHeight - this.padding)
            .attr("class", "quadrant-axis");

        // quadrant labels
        this.artboard
            .append("text")
            .attr("x", this.artboardWidth / 2)
            .attr("y", this.artboardUnit)
            .attr("class", "quadrant-label")
            .text("Flexibility & Discretion");

        this.artboard
            .append("text")
            .attr("x", this.artboardWidth)
            .attr("y", (this.artboardHeight / 2) + (this.artboardUnit *.33))
            .attr("class", "quadrant-label")
            .text("External Focus & Differentiation");

        this.artboard
            .append("text")
            .attr("x", this.artboardWidth / 2)
            .attr("y", this.artboardHeight)
            .attr("class", "quadrant-label")
            .text("Stability & Control");

        this.artboard
            .append("text")
            .attr("x", 0)
            .attr("y", (this.artboardHeight / 2) + (this.artboardUnit *.33))
            .attr("class", "quadrant-label")
            .text("Internal Focus & Integration");

        // chart content group
        const g = this.artboard
            .append("g")
            .attr("transform", `translate(${this.artboardWidth/2},${this.artboardHeight/2})`);

        // generate circular grid
        this.grid = this.generateGrid(g);

        // position/style grid
        this.configureGrid();

        // generate polygons
        this.polygon = this.generatePolygons(g);

        // position/style polygons
        this.configurePolygons();

        // generate axes
        this.axis = this.generateAxes(g);

        // position/style axes
        this.configureAxes();

        // generate points
        this.point = this.generatePoints(g);

        // position/style points
        this.configurePoints();

        // generate axis labels
        this.axisLabel = this.generateAxesLabels(g);

        // position/style axes labels
        this.configureAxesLabels();

        // generate grid labels
        this.gridLabel = this.generateGridLabels(g);

        // position/style grid labels
        this.configureGridLabels();

    }

};

export { RC };
export default RC;
